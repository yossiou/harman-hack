import config from './config.json';

const buildURL = (route, queryParams) => {
  const { IP, PORT } = config.config;
  return `http://${IP}:${PORT}/${route}?${queryParams}`;
};

export { buildURL };