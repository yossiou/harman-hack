import React from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/es/Button/Button';
import { buildURL } from '../utils.js';

class Input extends React.Component {
  hot = async () => {
    const URL = buildURL('temperature', 'diff=-1');
    console.log(`URL=${URL}`);
    await fetch(URL);
  }

  cold = async () => {
    const URL = buildURL('temperature', 'diff=1');
    console.log(`URL=${URL}`);
    await fetch(URL);
  }

  onClickHandler = (value) => async () => {
    console.log(`value =${value}`);
    const URL = buildURL('temperature', 'diff=' + value);
    console.log(`URL=${URL}`);
    await fetch(URL);
  }

  render() {
    return (
      <div>
        <Grid container direction='column'
              justify="center"
              alignItems="center">
          <Grid item style={{
            width: 200,
            height: 100,
            margin: 10
          }}>
            <Button variant="contained"
                    component="span"
                    color={'primary'}
                    style={{
                      width: 200,
                      height: 100
                    }}
                    onClick={this.onClickHandler(2)}
            >
              <div style={{ fontSize: 20 }}>I'm very cold!</div>
            </Button>
          </Grid>
          <Grid item style={{
            width: 200,
            height: 100,
            margin: 10
          }}>
            <Button variant="contained"
                    component="span"
                    color={'primary'}
                    style={{
                      width: 200,
                      height: 100
                    }}
                    onClick={this.onClickHandler(1)}
            >
              <div style={{ fontSize: 20}}>I'm cold!</div>
            </Button>
          </Grid>
          <Grid item style={{
            width: 200,
            height: 100,
            margin: 10
          }}>
            <Button variant="contained"
                    component="span"
                    color={'secondary'}
                    style={{
                      width: 200,
                      height: 100
                    }}
                    onClick={this.onClickHandler(-1)}
            >
              <div style={{ fontSize: 20 }}>I'm hot!</div>
            </Button>
          </Grid>
          <Grid item style={{
            width: 200,
            height: 100,
            margin: 10
          }}>
            <Button variant="contained"
                    component="span"
                    color={'secondary'}
                    style={{
                      width: 200,
                      height: 100
                    }}
                    onClick={this.onClickHandler(-2)}
            >
              <div style={{ fontSize: 20 }}>I'm very hot!</div>
            </Button>
          </Grid>
        </Grid>
      </div>
    )
  }
}

export default Input;
