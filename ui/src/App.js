import React, { Component } from 'react';
import Input from './components/Input.js';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import MaterialTheme from './style/MaterialTheme.js';
import AppBar from './components/AppBar.js';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <MuiThemeProvider theme={MaterialTheme}>
          <AppBar />
          <Input/>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default App;
