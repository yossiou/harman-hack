import RPi.GPIO as GPIO
import time
import sys
GPIO.setmode(GPIO.BOARD)

GPIO.setup(32, GPIO.OUT)

p = GPIO.PWM(32, 50)

p.start(7.5)
count = abs(int(sys.argv[1]))
try:
	while count:
		p.ChangeDutyCycle(6) # turn towards 90 degree
	        time.sleep(0.5) # sleep 1 second
       	 	p.ChangeDutyCycle(7.5)  # turn towards 0 degree
       	 	time.sleep(0.5) # sleep 1 second
		count = count - 1
except KeyboardInterrupt:
    p.stop()
    GPIO.cleanup()
