const express = require('express');
const server_config = require('../config.json').server_config;
const database_config = require('../config.json').database_config;
const algo = require('./algo.js');
const mysql = require('mysql');

const app = express();

const con = mysql.createConnection(database_config);
let endTime = 0;
let startTime = new Date().getTime();

app.get('/temperature', (req, res) => {
  console.log(req.query.diff);
  res.send();
  const sql_insert = `INSERT INTO Temp (temp) VALUES (${req.query.diff})`;
  console.log(`Query is: ${sql_insert}`);
  con.query(sql_insert);
  console.log('temperature was inserted');
});

function diffTime() {
  endTime = new Date().getTime();
  console.log(`startTime: ${startTime}`);
  console.log(`endTime: ${endTime}`);
  return endTime - startTime;
}

function calcAvg() {
  const sql_select = 'SELECT AVG (temp) FROM Temp';
  let avgValue = 0;
  return new Promise((resolve, reject) => {
    const avg = con.query(sql_select, (err, results, fields) => {
      avgValue = (results[0]['AVG (temp)']) ? results[0]['AVG (temp)'] : 0;
      console.log(`The average is ${avgValue}`);
      resolve(avgValue);
    });
  });
}

function deleteDataFromTable() {
  const command = 'DELETE from Temp';
  con.query(command);
}

setInterval(async () => {
  const avg = await calcAvg();
  deleteDataFromTable();
  startTime = new Date().getTime();
  endTime = 0;
  console.log(`avg :${avg}`);
  algo.runScript(avg);
}, 20000);

const port = server_config.PORT;
const ip = server_config.IP;


app.listen(port, ip, () => console.log(`Example app listening on port ${port}!`));
