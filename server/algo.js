const { spawn } = require('child_process');

const roundDiff = outDiff => {
  if (outDiff > 3) {
    outDiff = 3;
  } else {
    outDiff = Math.round(outDiff);
  }
  return outDiff;
};

function runScript(outDiff) {
  outDiff = roundDiff(outDiff);
  console.log(`outDiff=${outDiff}`);
  if (outDiff < 0) {
    console.log('DOWN');
    spawn('python', ['../motorAction/down.py', outDiff]);
  } else if (outDiff > 0) {
    console.log('UP');
    spawn('python', ['../motorAction/up.py', outDiff]);
  } else {
    console.log('Nothing changed');
  }
}
module.exports = { runScript };
